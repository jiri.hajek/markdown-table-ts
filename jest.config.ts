import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  collectCoverage: true,
  coverageDirectory: 'coverage',
  roots: ['test'],
  transform: { '^.+\\.ts$': 'ts-jest' },
  verbose: true,
};

export default config;
