import {
  Align,
  getMarkdownTable,
} from '../src/index';

const tableFromRowStrings = (rows: string[]): string => {
  const head = rows[0];
  const align = rows[1];
  const bodyArray = rows.slice(2);
  const nlBody = bodyArray.length ? `\n${bodyArray.join('\n')}` : '';

  return `${head}\n${align}${nlBody}`;
};

describe('getMarkdownTable', () => {
  describe('input validation', () => {
    test('should throw when params is undefined', () => {
      expect(() => {
        // @ts-expect-error
        getMarkdownTable(undefined);
      }).toThrow();
    });

    test('should throw when params is string', () => {
      expect(() => {
        // @ts-expect-error
        getMarkdownTable('hello');
      }).toThrow();
    });

    test('should throw when table is undefined', () => {
      expect(() => {
        // @ts-expect-error
        getMarkdownTable({});
      }).toThrow();
    });

    test('should throw when table head is undefined', () => {
      expect(() => {
        getMarkdownTable({
          // @ts-expect-error
          table: {
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
        });
      }).toThrow();
    });

    test('should throw when table has 0 headers', () => {
      expect(() => {
        getMarkdownTable({
          table: {
            head: [],
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
        });
      }).toThrow();
    });

    test('should throw when table body is undefined', () => {
      expect(() => {
        getMarkdownTable({
          // @ts-expect-error
          table: {
            head: ['a', 'b', 'c'],
          },
        });
      }).toThrow();
    });

    test('should not throw when table body has 0 columns', () => {
      expect(() => {
        getMarkdownTable({
          table: {
            head: ['a', 'b', 'c'],
            body: [],
          },
        });
      }).not.toThrow();
    });

    test('should throw when alignColumns is neither undefined or boolean', () => {
      expect(() => {
        getMarkdownTable({
          table: {
            head: ['a', 'b', 'c'],
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
          // @ts-expect-error
          alignColumns: 'hello',
        });
      }).toThrow();
    });

    test('should not throw when alignColumns is undefined', () => {
      expect(() => {
        getMarkdownTable({
          table: {
            head: ['a', 'b', 'c'],
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
        });
      }).not.toThrow();
    });

    test('should not throw when alignColumns is true', () => {
      expect(() => {
        getMarkdownTable({
          table: {
            head: ['a', 'b', 'c'],
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
          alignColumns: true,
        });
      }).not.toThrow();
    });

    test('should not throw when alignColumns is false', () => {
      expect(() => {
        getMarkdownTable({
          table: {
            head: ['a', 'b', 'c'],
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
          alignColumns: false,
        });
      }).not.toThrow();
    });

    test('should not throw when alignment is undefined', () => {
      expect(() => {
        getMarkdownTable({
          table: {
            head: ['a', 'b', 'c'],
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
        });
      }).not.toThrow();
    });

    test('should throw when alignment is truthy non-array object', () => {
      expect(() => {
        getMarkdownTable({
          table: {
            head: ['a', 'b', 'c'],
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
          // @ts-expect-error
          alignment: 1,
        });
      }).toThrow();
    });

    test('should throw when alignment contains non-Align object', () => {
      expect(() => {
        getMarkdownTable({
          table: {
            head: ['a', 'b', 'c'],
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
          // @ts-expect-error
          alignment: [Align.Left, Align.Right, 'hello'],
        });
      }).toThrow();
    });

    test('should throw when table head is not an array', () => {
      expect(() => {
        getMarkdownTable({
          table: {
            // @ts-expect-error
            head: 111,
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
        });
      }).toThrow();
    });

    test('should throw when table head contains a non-string value', () => {
      expect(() => {
        getMarkdownTable({
          table: {
            // @ts-expect-error
            head: ['1', '2', 3, '4'],
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
        });
      }).toThrow();
    });

    test('should throw when one of the rows in table body is not an array', () => {
      expect(() => {
        getMarkdownTable({
          table: {
            head: ['1', '2', '3'],
            body: [
              ['a', 'b', 'c'],
              // @ts-expect-error
              'row',
              ['d', 'e', 'f'],
            ],
          },
        });
      }).toThrow();
    });

    test('should throw when some column in table body contains a non-string value', () => {
      expect(() => {
        getMarkdownTable({
          table: {
            head: ['1', '2', '3'],
            body: [
              // @ts-expect-error
              ['a', 2, 'c'],
            ],
          },
        });
      }).toThrow();
    });
  });

  describe('tables', () => {
    test('more headers than columns on any row in body', () => {
      const got = getMarkdownTable({
        table: {
          head: ['1', '2', '3', '4'],
          body: [
            ['a'],
            [],
            ['b', 'c'],
          ],
        },
        alignColumns: false,
      });

      const want = tableFromRowStrings([
        '| 1 | 2 | 3 | 4 |',
        '| --- | --- | --- | --- |',
        '| a |  |  |  |',
        '|  |  |  |  |',
        '| b | c |  |  |',
      ]);

      expect(got).toBe(want);
    });

    test('less headers than columns on some row in body', () => {
      const got = getMarkdownTable({
        table: {
          head: ['1', '2'],
          body: [
            ['a'],
            [],
            ['b', 'c', 'd', 'e', 'f'],
          ],
        },
        alignColumns: false,
      });

      const want = tableFromRowStrings([
        '| 1 | 2 |  |  |  |',
        '| --- | --- | --- | --- | --- |',
        '| a |  |  |  |  |',
        '|  |  |  |  |  |',
        '| b | c | d | e | f |',
      ]);

      expect(got).toBe(want);
    });

    test('less headers than columns on all rows in body', () => {
      const got = getMarkdownTable({
        table: {
          head: ['1', '2'],
          body: [
            ['a', 'b', 'c', 'd', 'e'],
            ['f', 'g', 'h', 'i'],
            ['j', 'k', 'l'],
          ],
        },
        alignColumns: false,
      });

      const want = tableFromRowStrings([
        '| 1 | 2 |  |  |  |',
        '| --- | --- | --- | --- | --- |',
        '| a | b | c | d | e |',
        '| f | g | h | i |  |',
        '| j | k | l |  |  |',
      ]);

      expect(got).toBe(want);
    });

    test('same amount of columns across all rows', () => {
      const got = getMarkdownTable({
        table: {
          head: ['1', '2', '3'],
          body: [
            ['a', 'b', 'c'],
            ['d', 'e', 'f'],
          ],
        },
        alignColumns: false,
      });

      const want = tableFromRowStrings([
        '| 1 | 2 | 3 |',
        '| --- | --- | --- |',
        '| a | b | c |',
        '| d | e | f |',
      ]);

      expect(got).toBe(want);
    });

    test('one row in body has the same amount of columns as there are headers', () => {
      const got = getMarkdownTable({
        table: {
          head: ['1', '2', '3'],
          body: [
            ['a', 'b', 'c', 'd', 'e'],
            ['f', 'g', 'h'],
            ['i', 'j'],
            [],
          ],
        },
        alignColumns: false,
      });

      const want = tableFromRowStrings([
        '| 1 | 2 | 3 |  |  |',
        '| --- | --- | --- | --- | --- |',
        '| a | b | c | d | e |',
        '| f | g | h |  |  |',
        '| i | j |  |  |  |',
        '|  |  |  |  |  |',
      ]);

      expect(got).toBe(want);
    });

    test('0 rows in body', () => {
      const got = getMarkdownTable({
        table: {
          head: ['1', '2', '3'],
          body: [],
        },
        alignColumns: false,
      });

      const want = tableFromRowStrings([
        '| 1 | 2 | 3 |',
        '| --- | --- | --- |',
      ]);

      expect(got).toBe(want);
    });

    describe('alignment', () => {
      test('undefined alignment', () => {
        const got = getMarkdownTable({
          table: {
            head: ['1', '2', '3'],
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
          alignColumns: false,
        });

        const want = tableFromRowStrings([
          '| 1 | 2 | 3 |',
          '| --- | --- | --- |',
          '| a | b | c |',
          '| d | e | f |',
        ]);

        expect(got).toBe(want);
      });

      test('alignment.length < amount of columns', () => {
        const got = getMarkdownTable({
          table: {
            head: ['1', '2', '3'],
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
          alignColumns: false,
          alignment: [Align.Left, Align.Center],
        });

        const want = tableFromRowStrings([
          '| 1 | 2 | 3 |',
          '| :-- | :-: | --- |',
          '| a | b | c |',
          '| d | e | f |',
        ]);

        expect(got).toBe(want);
      });

      test('alignment.length = amount of columns', () => {
        const got = getMarkdownTable({
          table: {
            head: ['1', '2', '3'],
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
          alignColumns: false,
          alignment: [Align.Left, Align.Center, Align.Right],
        });

        const want = tableFromRowStrings([
          '| 1 | 2 | 3 |',
          '| :-- | :-: | --: |',
          '| a | b | c |',
          '| d | e | f |',
        ]);

        expect(got).toBe(want);
      });

      test('alignment.length > amount of columns', () => {
        const got = getMarkdownTable({
          table: {
            head: ['1', '2', '3'],
            body: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
          alignColumns: false,
          alignment: [Align.Left, Align.Center, Align.Right, Align.Left, Align.Left],
        });

        const want = tableFromRowStrings([
          '| 1 | 2 | 3 |',
          '| :-- | :-: | --: |',
          '| a | b | c |',
          '| d | e | f |',
        ]);

        expect(got).toBe(want);
      });

      test('only align left', () => {
        const got = getMarkdownTable({
          table: {
            head: ['1', '22', '3'],
            body: [
              ['a', 'b', 'c'],
              ['dd', 'e', 'feh'],
            ],
          },
          alignColumns: true,
          alignment: [Align.Left, Align.Left, Align.Left],
        });

        const want = tableFromRowStrings([
          '| 1   | 22  | 3   |',
          '| :-- | :-- | :-- |',
          '| a   | b   | c   |',
          '| dd  | e   | feh |',
        ]);

        expect(got).toBe(want);
      });

      test('only align center', () => {
        const got = getMarkdownTable({
          table: {
            head: ['1', '22', '3'],
            body: [
              ['a', 'b', 'c'],
              ['dd', 'e', 'feh'],
            ],
          },
          alignColumns: true,
          alignment: [Align.Center, Align.Center, Align.Center],
        });

        const want = tableFromRowStrings([
          '|  1  | 22  |  3  |',
          '| :-: | :-: | :-: |',
          '|  a  |  b  |  c  |',
          '| dd  |  e  | feh |',
        ]);

        expect(got).toBe(want);
      });

      test('only align right', () => {
        const got = getMarkdownTable({
          table: {
            head: ['1', '22', '3'],
            body: [
              ['a', 'b', 'c'],
              ['dd', 'e', 'feh'],
            ],
          },
          alignColumns: true,
          alignment: [Align.Right, Align.Right, Align.Right],
        });

        const want = tableFromRowStrings([
          '|   1 |  22 |   3 |',
          '| --: | --: | --: |',
          '|   a |   b |   c |',
          '|  dd |   e | feh |',
        ]);

        expect(got).toBe(want);
      });

      test('mixed alignments', () => {
        const got = getMarkdownTable({
          table: {
            head: ['1', '22', '3', '4'],
            body: [
              ['a', 'bbb', 'cc', 'c2'],
              ['dd', 'e', 'ff2', 'ff3'],
              ['ggg', 'hh', 'i', 'i2'],
            ],
          },
          alignColumns: true,
          alignment: [Align.Left, Align.Center, Align.Right, Align.None],
        });

        const want = tableFromRowStrings([
          '| 1   | 22  |   3 | 4   |',
          '| :-- | :-: | --: | --- |',
          '| a   | bbb |  cc | c2  |',
          '| dd  |  e  | ff2 | ff3 |',
          '| ggg | hh  |   i | i2  |',
        ]);

        expect(got).toBe(want);
      });

      test('Align.None should be equivalent to undefined alignment', () => {
        const table = {
          head: ['1', '22', '33333333333', '4'],
          body: [
            ['aaaaaaaaaaaaaaa', 'b', 'cc', 'c2'],
            ['dd', 'e', 'ff2', 'ff3'],
            ['ggg', 'h', 'i', 'i2'],
          ],
        };

        const got = getMarkdownTable({
          table,
          alignColumns: true,
          alignment: [Align.None, Align.None, Align.None, Align.None],
        });

        const want = getMarkdownTable({
          table,
          alignColumns: true,
        });

        expect(got).toBe(want);
      });
    });

    describe('alignColumns', () => {
      test('long columns in body', () => {
        const got = getMarkdownTable({
          table: {
            head: ['1', '2', '3'],
            body: [
              ['hey', 'i', 'amatests'],
              ['uitedoin', 'g', 'some'],
              ['ver', 'yu', 'sefulstuffoverhere:)'],
            ],
          },
          alignColumns: true,
        });

        const want = tableFromRowStrings([
          '| 1        | 2   | 3                    |',
          '| -------- | --- | -------------------- |',
          '| hey      | i   | amatests             |',
          '| uitedoin | g   | some                 |',
          '| ver      | yu  | sefulstuffoverhere:) |',
        ]);

        expect(got).toBe(want);
      });

      test('long headers', () => {
        const got = getMarkdownTable({
          table: {
            head: ['longheader', 'evenlongerheader', 'thelongestheaderthatseverlived'],
            body: [
              ['your', 'apiis', 'ahallofshame'],
              ['you', 'give', 'rest'],
              ['a', 'bad', 'name'],
            ],
          },
          alignColumns: true,
        });

        const want = tableFromRowStrings([
          '| longheader | evenlongerheader | thelongestheaderthatseverlived |',
          '| ---------- | ---------------- | ------------------------------ |',
          '| your       | apiis            | ahallofshame                   |',
          '| you        | give             | rest                           |',
          '| a          | bad              | name                           |',
        ]);

        expect(got).toBe(want);
      });
    });
  });
});
