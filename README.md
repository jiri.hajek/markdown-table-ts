# markdown-table-ts
[![pipeline](https://img.shields.io/gitlab/pipeline/jiri.hajek/markdown-table-ts/master)](https://gitlab.com/jiri.hajek/markdown-table-ts/-/pipelines?page=1&scope=all&ref=master)
[![coverage](https://gitlab.com/jiri.hajek/markdown-table-ts/badges/master/coverage.svg)](https://gitlab.com/jiri.hajek/markdown-table-ts/-/commits/master)
[![license](https://img.shields.io/npm/l/markdown-table-ts)](https://gitlab.com/jiri.hajek/markdown-table-ts/-/blob/master/LICENSE)
[![version](https://img.shields.io/npm/v/markdown-table-ts)](https://www.npmjs.com/package/markdown-table-ts)
[![downloads](https://img.shields.io/npm/dt/markdown-table-ts)](https://www.npmjs.com/package/markdown-table-ts)

[markdown-table-ts](https://www.npmjs.com/package/markdown-table-ts) is a zero-dependency library for generating Markdown tables. Since it's written in TypeScript, it comes with type declarations out of the box.

## Installation
Grab [markdown-table-ts](https://www.npmjs.com/package/markdown-table-ts) from **npm** with your favorite package manager.

```sh
# npm
npm install markdown-table-ts

# yarn
yarn add markdown-table-ts
```

## Quick start
```typescript
// Node.js
// const { Align, getMarkdownTable } = require('markdown-table-ts');

// Node.js with TypeScript
import { Align, getMarkdownTable } from 'markdown-table-ts';

const table = getMarkdownTable({
  table: {
    head: ['ID', 'Name', 'Age'],
    body: [
      ['1', 'John',  '26'],
      ['2', 'Bob',   '25'],
      ['3', 'Alice', '23'],
    ],
  },
  alignment: [Align.Left, Align.Center, Align.Right],
});

console.log(table);

// Output:
// | ID  | Name  | Age |
// | :-- | :---: | --: |
// | 1   | John  |  26 |
// | 2   |  Bob  |  25 |
// | 3   | Alice |  23 |
```

## API
### `getMarkdownTable(params: GetTableInput): string`
* generates the corresponding Markdown table based on `params`
* does **not** append newline to the end of the resulting table
* throws a `MarkdownTableError` when something goes wrong.

The underlying structure of `GetTableInput` looks like this.
```typescript
enum Align {
  Left   = 'left',
  Right  = 'right',
  Center = 'center',
  None   = 'none',
}

type Column = string;
type Row    = Column[];

interface Table {
  head: Row,
  body: Row[],
}

interface GetTableInput {
  table:         Table,

  alignColumns?: boolean,
  alignment?:    Align[],
}
```

#### `params.table.head`
* table headers
* must be an array of strings
* **cannot** be empty

##### Examples
```typescript
// OK
['header1', 'header2', 'header3']

// Error
undefined
[]
```

#### `params.table.body`
* table body
* must be an array whose elements are arrays of strings
* **can** be empty

##### Examples
```typescript
// OK
['column1', 'column2', 'column3']
[]

// Error
undefined
```

#### `params.alignColumns`
* whether to vertically align columns (`|`)
* must be boolean or undefined (in which case it will default to **true**)

##### Examples
```typescript
// OK
true
false
undefined

// Error
'yes'
1

```

##### `alignColumns` on/off
```typescript
import { Align, Table, getMarkdownTable } from 'markdown-table-ts';

const myTable: Table = {
  head: ['first header', 'second header', 'very very very long header'],
  body: [
    ['1',                      'John',  '26'],
    ['very very very long id', 'Bob',   '25'],
    ['3',                      'Alice', '23'],
  ],
};

const myAlignment: Align[] = [Align.Center, Align.None, Align.Right];

console.log(getMarkdownTable({
  table: myTable,
  alignment: myAlignment,

  alignColumns: true,
}));

console.log(getMarkdownTable({
  table: myTable,
  alignment: myAlignment,

  alignColumns: false,
}));

// alignColumns on
// |      first header      | second header | very very very long header |
// | :--------------------: | ------------- | -------------------------: |
// |           1            | John          |                         26 |
// | very very very long id | Bob           |                         25 |
// |           3            | Alice         |                         23 |

// alignColumns off
// | first header | second header | very very very long header |
// | :-: | --- | --: |
// | 1 | John | 26 |
// | very very very long id | Bob | 25 |
// | 3 | Alice | 23 |
```

#### `params.alignment`
* defines how to align each of the columns
* must be an array of `Align` which is one of
  * `Align.Left` (`:----`)
  * `Align.Right` (`----:`)
  * `Align.Center` (`:---:`)
  * `Align.None` (`-----`)
* defaults to `Align.None`

##### Examples
```typescript
// OK
[Align.Left, Align.Right, Align.Center, Align.None]

// Not recommended but this will also pass if you turn off type checking
['left', 'right', 'center', 'none']

// Error
[Align.left, 1, 'hello']
```

## Edge cases
### Some rows have more columns than others
When some of the rows (table head included) have more columns than others, the resulting table will have as many columns as the row with the most columns. Undefined columns will be empty.

```typescript
import { getMarkdownTable } from 'markdown-table-ts';

const table = getMarkdownTable({
  table: {
    head: ['ID', 'Name', 'Age'],
    body: [
      ['1', 'John',  '26', 'more data', 'even more data'],
      ['2', 'Bob',   '25'],
      ['3', 'Alice', '23', 'extra column'],
    ],
  },
});

console.log(table);

// Output:
// | ID  | Name  | Age |              |                |
// | --- | ----- | --- | ------------ | -------------- |
// | 1   | John  | 26  | more data    | even more data |
// | 2   | Bob   | 25  |              |                |
// | 3   | Alice | 23  | extra column |                |
```

### Length of `alignment` array is less than amount of columns in the table
When there are less elements in the `params.alignment` array than there are columns in the resulting table, `getMarkdownTable` will still succeed and columns with undefined alignment will default to `Align.None` (`-----`), e.g. when there are 4 columns in the table and the length of `params.alignment` is **2**, the first **2** columns will use alignment from `params.alignment` and the rest of the columns will default to `Align.None` (`-----`).

```typescript
import { Align, getMarkdownTable } from 'markdown-table-ts';

const table = getMarkdownTable({
  table: {
    head: ['ID', 'Name', 'Age', 'Country'],
    body: [
      ['1', 'John',  '26', 'Czechia'],
      ['2', 'Bob',   '25', 'United States'],
      ['3', 'Alice', '23', 'Denmark'],
    ],
  },
  alignment: [Align.Center, Align.Left],
});

console.log(table);

// Output:
// | ID  | Name  | Age | Country       |
// | :-: | :---- | --- | ------------- |
// |  1  | John  | 26  | Czechia       |
// |  2  | Bob   | 25  | United States |
// |  3  | Alice | 23  | Denmark       |
```
